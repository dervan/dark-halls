START { 
    js=0;
    included_minified = 0
} 

/\/\/JavaScript/ { js=1 } 

// { 
    if ( js!=1 ) { 
        print $0
    }else if (included_minified == 0){
        print "\"./js/car-testing-center.min.js\",";
        included_minified=1;
    }
} 

/\/\/Images/ {print $0; js=0}
