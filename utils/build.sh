jsFiles=`awk -f ./utils/list_all_js_files.awk ./js/loadManifest.js`

rm -rf ./build/*
cp -r css ./build/

mkdir ./build/img
cp -r img/sprites.* ./build/img/
cp -r img/*.jpg ./build/img/
cp -r img/*.png ./build/img/

cp -r maps ./build
rm -r ./build/maps/*/*.xcf
cp -r extras ./build
cp -r sounds ./build
cp -r data ./build

mkdir ./build/js
cp -r js/vendor ./build/js

awk -f ./utils/generate_min_loadManifest.awk ./js/loadManifest.js > ./build/js/loadManifest.js

cp index.html ./build
cp manifest.json ./build

cp ./js/main.js ./build/js/main.js

closure --compilation_level SIMPLE_OPTIMIZATIONS --js $jsFiles --js_output_file ./build/js/car-testing-center.min.js

cd ./build
zip -r dark-halls.zip css data img index.html js maps sounds
