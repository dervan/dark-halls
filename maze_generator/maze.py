#!/usr/bin/python2
import sys
import random
import json
import numpy
from numpy.random import random_integers as rand
import matplotlib.pyplot as pyplot
 
def maze(width=61, height=31, complexity=.075, density=1.0):
    # Only odd shapes
    shape = ((height // 2) * 2 + 1, (width // 2) * 2 + 1)
    # Adjust complexity and density relative to maze size
    complexity = int(complexity * (5 * (shape[0] + shape[1])))
    density    = int(density * (shape[0] // 2 * shape[1] // 2))
    # Build actual maze
    Z = numpy.zeros(shape, dtype=int)
    # Fill borders
    Z[0, :] = Z[-1, :] = 1
    Z[:, 0] = Z[:, -1] = 1
    # Make aisles
    for i in range(density):
        x, y = rand(0, shape[1] // 2) * 2, rand(0, shape[0] // 2) * 2
        Z[y, x] = 1
        for j in range(complexity):
            neighbours = []
            if x > 1:             neighbours.append((y, x - 2))
            if x < shape[1] - 2:  neighbours.append((y, x + 2))
            if y > 1:             neighbours.append((y - 2, x))
            if y < shape[0] - 2:  neighbours.append((y + 2, x))
            if len(neighbours):
                y_,x_ = neighbours[rand(0, len(neighbours) - 1)]
                if Z[y_, x_] == 0:
                    Z[y_, x_] = 1
                    Z[y_ + (y - y_) // 2, x_ + (x - x_) // 2] = 1
                    x, y = x_, y_
    return Z

def scanDown(tab,i,j):
    s = j
    while s<len(tab[i])-1 and tab[i][s+1]:
        s=s+1
    if s != j:
        return [[i,j],[i,s]]
    else:
        if (i == 0 or tab[i-1][j]==0) and (i == len(tab) or tab[i+1][j]==0):
            return [[i,j],[i,j]]
        else:
            return 0

def scanRight(tab,i,j):
    s = i
    while s<len(tab)-1 and tab[s+1][j]:
        s=s+1
    if s != i:
        return [[i,j],[s,j]]
    else:
        return 0

def compress(Z):
    wallList = []
    for i in range(len(Z)):
        for j in range(len(Z[i])):
            if Z[i][j]:
                if i == 0 or Z[i-1][j] == 0:
                    t = scanRight(Z,i,j)
                    if t:
                        wallList.append(t)
                if j==0 or Z[i][j-1] == 0:
                    t = scanDown(Z,i,j)
                    if t:
                        wallList.append(t)
    return wallList


def printMaze(m):
    pyplot.figure(figsize=(len(m)+2, len(m[0])+2))
    pyplot.imshow(m, cmap=pyplot.cm.binary, interpolation='nearest')
    pyplot.xticks([]), pyplot.yticks([])
    pyplot.show()
    return

def main():
    if len(sys.argv) < 5:
        print 'Usage %s HEIGHT WIDTH COMPLEXITY DENSITY' % sys.argv[0]
        return 1
    else:
        height = (int(sys.argv[1])//2)*2 + 1
        width = (int(sys.argv[2])//2)*2 + 1

        complexity = float(sys.argv[3])
        density = float(sys.argv[4])
        m = maze(height,width,complexity,density)
        z = compress(m)
        random.seed()
        sx = random.randrange(1,height//3,2)
        sy = random.randrange(1,width//3,2)
        ex = random.randrange(2*height//3,height,2)
        ey = random.randrange(2*width//3,width,2)
        j = json.dumps({"start_pos": [sx,sy],"end_pos": [ex,ey],
            "height":height,"width":width,"floor_type":2,"wall_thickness":0.5,"keys":[],"doors":[],"segments":z});
        print j
        printMaze(m)
        return 0

if __name__ == '__main__':
    main()
