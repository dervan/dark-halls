var loadManifest = [
    //Vendor
    "js/vendor/lodash.min.js",
    "js/vendor/easeljs-0.7.1.min.js",
    "js/vendor/Box2dWeb-2.1.a.3.min.js",
    //JavaScript
    "js/utils.js",
    "js/game.js",
    "js/menu.js",
    "js/imageManager.js",
    "js/models/player.js",
    "js/models/key.js",
    "js/models/door.js",
    "js/models/maze.js",
    "js/models/viewport.js",
    "js/views/preloader.js",
    "js/views/player.js",
    "js/views/key.js",
    "js/views/door.js",
    "js/views/game.js",
    "js/views/maze.js",
    "js/views/light_mask.js",
    //Images
    {src: "img/mask.png", id: "mask"},
    {src: "img/sprites.png", id: "spritesheet"},
    {src: "img/sprites.json", id: "spritesheet-data"},
    {src: "img/floor1.png", id: "floor1"},
    {src: "img/floor2.png", id: "floor2"},
    {src: "img/floor3.png", id: "floor3"},
    //Data
    {src: "data/levels/level0.json", id: "level0"},
    {src: "data/levels/level1.json", id: "level1"},
    {src: "data/levels/level2.json", id: "level2"},
    {src: "data/levels/level3.json", id: "level3"}
];
