
var utils = {};
_.extend(utils, {
    scale: 30,
    m2px: function(m){
        return m*this.scale;
    },
    px2m: function(px){
        return px/this.scale;
    },
    rad2deg: function(rad){
        return (rad*180/Math.PI)%360;
    },
    deg2rad: function(deg){
        return (deg*Math.PI/180)%(2*Math.PI);
    },
    inch2m: function(inch){
        return inch*0.0254;
    },
    // returns sing of number 
    sign: function(x) {
        return x ? x < 0 ? -1 : 1 : 0; 
    },
    stripSuffix: function(string, suffix){
        var lastIndex = string.lastIndexOf(suffix);
        if(lastIndex == -1) return string;
        return string.substr(0,lastIndex);
    },
    img2canvas: function(img){
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
        return canvas;
    },
    createCookie: function(name, value, days){
        var expires;
        if(days === undefined) days = 30;
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
        document.cookie = name+"="+value+expires+"; path=/";
    },
    readCookie: function(name){
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    eraseCookie: function(name){
        this.createCookie(name, "", -1);
    }
});
