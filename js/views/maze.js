var MazeView = function(stage, model, viewport) {
    this.model = model;
    this.stage = stage;
    this.color = '#000';
    this.key_views = Array();
    this.door_views = Array();
    this.viewport = viewport;
};

_.extend(MazeView.prototype, {
    draw: function(){

        /**
         * Unfortunately beginBitmapFill does not accept Easeljs `Graphics` 
         * class, so we cannot use Sprites here and we have to load
         * floors as a separate images.
         **/
        var bg_img = new Image();
        bg_img.src = "img/floor" + this.model.floor_type + ".png";
        var bg_shape = new createjs.Shape();
        
        bg_shape.graphics.beginStroke("#f00")
                .beginBitmapFill(bg_img, "repeat")
                .drawRect(0,0,
                        utils.m2px(this.model.width-2*(1-this.model.wall_thickness)),
                        utils.m2px(this.model.height-2*(1-this.model.wall_thickness)));
        this.stage.addChild(bg_shape);

        var segments =  this.model.list_segments();
        var i;
        for(i=0;i<segments.length;i++){
            x1=utils.m2px(segments[i][0][0]-0.5*this.model.wall_thickness);
            y1=utils.m2px(segments[i][0][1]-0.5*this.model.wall_thickness);
            x2=utils.m2px(segments[i][1][0]+0.5*this.model.wall_thickness);
            y2=utils.m2px(segments[i][1][1]+0.5*this.model.wall_thickness);
            
            var wall = new createjs.Shape();
            wall.graphics.beginFill(this.color).drawRect(x1,y1,x2-x1,y2-y1);
            this.stage.addChild(wall);
        }

        for(i = 0; i < this.model.doors.length; ++i) {
            this.door_views.push(new DoorView(this.stage, this.model.doors[i]));
        }

        for(i = 0; i < this.model.keys.length; ++i) {
            this.key_views.push(new KeyView(this.stage, this.model.keys[i], this.viewport));
        }
    },

    update: function() {
        var i;
        for(i = this.door_views.length - 1; i >= 0; --i) {
            if(this.door_views[i].model.opened) {
                this.door_views[i].destroy_door_view();
                this.door_views.splice(i, 1);
            }
        }
        for(i = this.key_views.length - 1; i >= 0; --i) {
            if(this.key_views[i].model.used) {
                this.key_views[i].destroy_key_view();
                this.key_views.splice(i, 1);
            }
        }
        for(i = 0; i < this.key_views.length; ++i) {
            this.key_views[i].update();
        }
    }
});
