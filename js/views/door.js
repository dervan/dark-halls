var DoorView = function(stage, model) {
    this.model = model;
    this.stage = stage;
    this.no = this.model.no;

    this.doorBitmap = new createjs.Bitmap("img/door" + this.no.toString() + ".png");
    this.doorBitmap.x = utils.m2px(this.model.get_x());
    this.doorBitmap.y = utils.m2px(this.model.get_y());
    this.doorBitmap.regX = 15;
    this.doorBitmap.regY = 15;
    this.doorBitmap.snapToPixel = true;
    this.doorBitmap.mouseEnabled = false;

    stage.addChild(this.doorBitmap);
};

_.extend(DoorView.prototype, {

    destroy_door_view: function() {
        this.stage.removeChild(this.doorBitmap);
    },

    update: function() {
    }
});

