var KeyView = function(stage, model, viewport) {
    this.model = model;
    this.stage = stage;
    this.viewport = viewport;
    this.no = this.model.no;

    this.keyBitmap = new createjs.Bitmap("img/key" + this.no.toString() + ".png");
    this.keyBitmap.x = utils.m2px(this.model.get_x());
    this.keyBitmap.y = utils.m2px(this.model.get_y());
    this.keyBitmap.regX = 15;
    this.keyBitmap.regY = 6;
    this.keyBitmap.snapToPixel = true;
    this.keyBitmap.mouseEnabled = false;

    stage.addChild(this.keyBitmap);
};

_.extend(KeyView.prototype, {

    destroy_key_view: function() {
        this.stage.removeChild(this.keyBitmap);
    },

    update: function() {
        if(!this.model.taken) {
            this.keyBitmap.rotation = utils.rad2deg(this.model.get_rotation());
            this.keyBitmap.x = utils.m2px(this.model.get_x());
            this.keyBitmap.y = utils.m2px(this.model.get_y());
        }
        else if(!this.model.used) {
            this.keyBitmap.rotation = utils.rad2deg(utils.rad2deg(-45));
            var o = this.viewport.topLeft();
            this.keyBitmap.x = o[0]+20 + 10 * this.no;
            this.keyBitmap.y = o[1]+20;
        }
    }
});

