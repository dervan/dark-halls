var PlayerView = function(stage, model) {
    this.model = model;
    this.stage = stage;

    var sprite_sheet_data = {
        images: ["img/walk_anim.png"],
        frames: {width:40, height:40},
        animations: {
            idle: {
                frames: [0]
            },
            walk: {
                frames: [1,2,3,4,5,6,7],
                speed: 0.4
            },
            walk_alt: {
                frames: [5,6,7,1,2,3,4],
                speed: 0.4
            },
            walk_backward: {
                frames: [7,6,5,4,3,2,1],
                speed: 0.25
            },
            walk_backward_alt: {
                frames: [4,3,2,1,7,6,5],
                speed: 0.25
            }
        }
    };
    this.anim_alt = false;

    var spriteSheet = new createjs.SpriteSheet(sprite_sheet_data);
    this.playerBitmap = new createjs.Sprite(spriteSheet, "idle");

    this.playerBitmap.name = "player";
    this.playerBitmap.x = utils.m2px(this.model.get_x());
    this.playerBitmap.y = utils.m2px(this.model.get_y());
    this.playerBitmap.regX = 20;
    this.playerBitmap.regY = 20;
    this.playerBitmap.snapToPixel = true;
    this.playerBitmap.mouseEnabled = false;

    stage.addChild(this.playerBitmap);
};

_.extend(PlayerView.prototype, {

    choose_animation: function() {
        if(this.model.is_moving() && this.model.get_velocity_length() > 1) {
            var FPS = 30;
            var vel = this.model.get_velocity_length();
            var anim = this.playerBitmap.currentAnimation;

            if(this.model.forward && anim != "walk" && anim != "walk_alt") {
                (!this.anim_alt) ? this.playerBitmap.gotoAndPlay("walk") : this.playerBitmap.gotoAndPlay("walk_alt");
                this.anim_alt = !this.anim_alt;
            }
            
            if(this.model.backward && anim != "walk_backward" && anim != "walk_backward_alt") {
                (!this.anim_alt) ? this.playerBitmap.gotoAndPlay("walk_backward") : this.playerBitmap.gotoAndPlay("walk_backward_alt");
                this.anim_alt = !this.anim_alt;
            }

            if(this.playerBitmap.currentAnimation in ["walk", "walk_alt"])
                this.playerBitmap.framerate = Math.max(8, vel * FPS / this.model.max_forward_velocity);
            else if(this.playerBitmap.currentAnimation in ["walk_backward", "walk_backward_alt"])
                this.playerBitmap.framerate = Math.max(8, vel * FPS / this.model.max_backward_velocity);
        }
        else this.playerBitmap.gotoAndPlay("idle");
    },

    update: function() {
        this.playerBitmap.rotation = utils.rad2deg(this.model.get_rotation()) + utils.rad2deg(67.5);
        this.playerBitmap.x = utils.m2px(this.model.get_x());
        this.playerBitmap.y = utils.m2px(this.model.get_y());
        this.choose_animation();
    }
});

