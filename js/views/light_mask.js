var LightMask = function(stage, light_source) {
    this.stage = stage;
    this.light_source = light_source;
    this.draw();
};

_.extend(LightMask.prototype, {
    draw: function() {
        var mask_img = new Image();
        mask_img.src = "img/mask.png";
        this.mask = new createjs.Shape();
        this.mask.graphics.beginBitmapFill(mask_img,"no-repeat").drawRect(0,0,500,500);
        this.mask.graphics.beginFill("#000").drawRect(5,5,3500,-3500)
                                            .drawRect(495,5,3500,3500)
                                            .drawRect(5,495,-3500,-3500)
                                            .drawRect(495,495,-3500,3500);
        this.stage.addChild(this.mask);

    },
    update: function() {
        this.mask.x = utils.m2px(this.light_source.get_x())-250;
        this.mask.y = utils.m2px(this.light_source.get_y())-250;
    }
});
