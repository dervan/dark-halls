var GameView = function(viewport) {
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.viewport = viewport;

    document.body.appendChild(this.canvas);

    this.stage = new createjs.Stage(this.canvas);
    this.stage.snapPixelsEnabled = true;

    this.debug = false;

    var self = this;
    var lastResizeCanvasCall = 0;
    var resizeCanvasTimeout = null;
    this.resizeCanvas = function(){
        var now = new Date();
        if(self === null || self.canvas.parentElement === null){
            self=null;
            return;
        }
        if(now - lastResizeCanvasCall < 500){
            clearTimeout(resizeCanvasTimeout);
            resizeCanvasTimeout = setTimeout(self.resizeCanvas, 500 - (now - lastResizeCanvasCall));
        }
        self.canvas.width = viewport.getWidth();
        self.canvas.height = viewport.getHeight();
        //self.render();
        lastResizeCanvasCall = new Date();
    };
};

_.extend(GameView.prototype, {
    canvas: null,
    ctx: null,
    stage: null,
    show: function() {
        window.addEventListener('resize', this.resizeCanvas, false);
        this.resizeCanvas();
    },
    hide: function() {
		this.canvas.parentNode.removeChild(this.canvas);
        this.canvas = null;
    },
    debug_draw: function(world) {
        world.DrawDebugData();
    },
    debug_draw_setup: function(world) {
        var debugDraw = new Box2D.Dynamics.b2DebugDraw();
        debugDraw.SetSprite(this.ctx);
        debugDraw.SetDrawScale(utils.scale);
        debugDraw.SetFillAlpha(0.3);
        debugDraw.SetLineThickness(1.0);
        debugDraw.SetFlags(Box2D.Dynamics.b2DebugDraw.e_shapeBit | Box2D.Dynamics.b2DebugDraw.e_jointBit);
        world.SetDebugDraw(debugDraw);
    },
    update: function(world, event) {
        var offset_x = utils.m2px(-this.viewport.x) + this.canvas.width/2;
        var offset_y = utils.m2px(-this.viewport.y) + this.canvas.height/2;
        
        if(this.debug) {
            this.ctx.save();
            this.ctx.clearRect(0, 0, 
                    this.canvas.width, this.canvas.height);
            this.ctx.translate(offset_x, offset_y); 
            this.ctx.scale(this.scale, this.scale);
            this.debug_draw(world);
            this.ctx.restore();
        }else{
            this.stage.update(event);
            this.stage.setTransform(offset_x, offset_y,
                                    this.scale, this.scale);
        }
    },
    set_debug: function(debug) {
        this.debug = debug;
    },
    toggle_debug: function() {
        this.set_debug(!this.debug);
    }
});
