var Game = {};

_.extend(Game, {
    start_game: function() {
        console.log("New Game");
        Menu.hide(); 

        var b2Vec2 = Box2D.Common.Math.b2Vec2,
            b2Fixture = Box2D.Dynamics.b2Fixture,
            b2World = Box2D.Dynamics.b2World,
            b2MassData = Box2D.Collision.Shapes.b2MassData,
            b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape,
            b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

        var world = new b2World(
                 new b2Vec2(0, 0), //gravity
                 true              //allow sleep
        );

        var maze = new Maze(world, loadQueue.getResult('level2'));
        var p = new Player(world, maze.start_pos[0], maze.start_pos[1]);
        
        var viewport = new Viewport(p);
        var game_view = new GameView(viewport);

        var mv = new MazeView(game_view.stage, maze, viewport);
        mv.draw();
        
        var pv = new PlayerView(game_view.stage, p);

        var light_mask = new LightMask(game_view.stage, p);

        game_view.show();
        

        var FPS = 30;

        game_view.debug_draw_setup(world);

        function handleTick(event) {
            p.update();
            maze.update();

            world.Step(
                 1 / FPS,   //frame-rate
                 10,       //velocity iterations
                 10       //position iterations
            );
            world.ClearForces();

            pv.update();
            mv.update();
            game_view.x = p.get_x();
            game_view.y = p.get_y();
            viewport.update();
            light_mask.update();
            game_view.update(world, event);
        }
		
        createjs.Ticker.setFPS(FPS);
        createjs.Ticker.addEventListener("tick", handleTick);


        var contact_listener = new Box2D.Dynamics.b2ContactListener();
        contact_listener.BeginContact = function(contact) {
            var objA = contact.GetFixtureA().GetBody().GetUserData();
            var objB = contact.GetFixtureB().GetBody().GetUserData();
            if(objA !== null && objB !== null) {
                if(typeof objA.on_collision === 'function') objA.on_collision(objB);
                if(typeof objB.on_collision === 'function') objB.on_collision(objA);
            }
        };
        world.SetContactListener(contact_listener);


        window.addEventListener("keydown", onKeyDown, false);
        function onKeyDown(e) {
            switch(e.keyCode) {
                case 37: //left
                    { p.left = true; break; }
                case 38: //up
                    { p.forward = true; break; }
                case 39: //right
                    { p.right = true; break; }
                case 40: //down
                    { p.backward = true; break; }
                case 68: //'d'
                    { game_view.toggle_debug(); break; }
            }
        }

        window.addEventListener("keyup", onKeyUp, false);
        function onKeyUp(e) {
            switch(e.keyCode) {
                case 37: //left
                    { p.left = false; break; }
                case 38: //up
                    { p.forward = false; break; }
                case 39: //right
                    { p.right = false; break; }
                case 40: //down
                    { p.backward = false; break; }
            }
        }
    }
});
