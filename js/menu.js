var Menu = {};
_.extend(Menu,{
	menuNode: null,

	buttons: Array(Array("newGameButton" ,"New Game" ,function(){Game.start_game();}), 
				   Array("loadGameButton","Load Game",function(){console.log("Load Game");}),
				   Array("creditsButton" ,"Credits"	 ,function(){this.credits();})
                   ),
	
	show: function(){
		if(this.menuNode !== null)return;

		this.menuNode = document.createElement('div');
		this.menuNode.setAttribute('id','menu');
		

		var levelNum =  document.createElement('input');
		levelNum.setAttribute('class','button');
		levelNum.setAttribute('id','levelNum');
		levelNum.setAttribute('placeholder',("Level Number"));
	  	this.menuNode.appendChild(levelNum);

		for(var i=0; i<this.buttons.length;i++){
			
			var newButton = document.createElement('a');
			newButton.setAttribute('class', 'button');
			newButton.setAttribute('id', this.buttons[i][0]);
			newButton.onclick = _.bind(this.buttons[i][2], this);
			newButton.innerHTML = this.buttons[i][1];

			this.menuNode.appendChild(newButton);
		}
		document.body.appendChild(this.menuNode);
	},

	hide: function(){
		this.menuNode.parentNode.removeChild(this.menuNode);
		this.menuNode=null;
	},

    credits: function() {
		var creditsNode = document.createElement('div');
        creditsNode.setAttribute('class', 'credits');
		creditsNode.innerHTML = "<h2>Programming:</h2><ul>" +
                                "<li><a href='http://mbarcis.net'>Michał Barciś</a></li>" +
                                "<li>Michał Jagielski</li>" +
                                "<li>Wojciech Kuprianowicz</li>"+
                                "</ul><h2>Art:</h2><ul>" +
                                "<li><a href='http://opengameart.org/users/bart'>Bart</a> from <a href='http://opengameart.org/'>opengameart.org</a> " +
                                "(thanks for awesome background textures!)</li>" +
                                "<li><a href='http://www.flickr.com/photos/plussed/'>Chris</a> from <a href='http://www.flickr.com/'>flickr.com</a> " +
                                "(the <a href='http://www.flickr.com/photos/plussed/8448624929/'>walking animation</a>)</li>" +
                                "</ul>";
        creditsNode.onclick = function() {
		    creditsNode.parentNode.removeChild(creditsNode);
    		creditsNode = null;
        };
        document.body.appendChild(creditsNode);
    },

});
	
