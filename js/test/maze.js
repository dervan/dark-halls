describe('Maze', function(){
    describe('attributes', function(){
        it('unitialized shouldn\'t have pointer to maze node in DOM before initializing',function(){
			var maze = new Maze();
			(maze.maze_node === null).should.be.true; // jshint ignore:line
			});
		it('should contain height attribute with initial 0 value',function(){
			var maze = new Maze();
			maze.should.containEql({'height':0});
			 });
		it('should contain width attribute with initial 0 value',function(){
			var maze = new Maze();
			maze.should.containEql({'width':0});
			 });

		it('should contain wall_thickness attribute with initial 1 value',function(){
			var maze = new Maze();
			maze.should.containEql({'wall_thickness':1});
			 });
		it('should contain segment [[0,0],[1,1]] after adding it to segments lists',function(){
			var maze = new Maze();
			maze.add_segment([[0,0],[1,1]]);
			(maze.list_segments()).should.containEql([[0,0],[1,1]]);
		});
		});
	describe('initialization from JSON',function(){
		it('should be initialized after loading maze from string with JSON object',function(){
			var simpleJSON = '{ "height": 10, "width": 10, "wall_thickness": 1, "segments": [ [[0,0],[0,10]], [[0,0],[10,0]], [[0,10],[10,10]], [[10,0],[10,10]], [[2,2],[8,8]] ] }';
            simpleJSON = JSON.parse(simpleJSON);
			maze = new Maze();
			maze.load_maze(simpleJSON);
			maze.width.should.be.equal(10);
			maze.height.should.be.equal(10);
			maze.wall_thickness.should.be.equal(1);
			maze.list_segments().should.containDeep([ [[0,0],[0,10]], [[0,0],[10,0]], [[0,10],[10,10]], [[10,0],[10,10]], [[2,2],[8,8]] ]);
		});
		it('should be initialized properly after loading few times maze from string with JSON object',function(){
			var simpleJSON = '{ "height": 10, "width": 10, "wall_thickness": 1, "segments": [ [[0,0],[0,10]], [[0,0],[10,0]], [[0,10],[10,10]], [[10,0],[10,10]], [[2,2],[8,8]] ] }';
            simpleJSON = JSON.parse(simpleJSON);
			maze = new Maze();
			maze.load_maze(simpleJSON);
			maze.load_maze(simpleJSON);
			maze.load_maze(simpleJSON);
			maze.width.should.be.equal(10);
			maze.height.should.be.equal(10);
			maze.wall_thickness.should.be.equal(1);
			maze.list_segments().should.containDeep([ [[0,0],[0,10]], [[0,0],[10,0]], [[0,10],[10,10]], [[10,0],[10,10]], [[2,2],[8,8]] ]);
		});
	});
		
});
