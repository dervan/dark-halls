describe('Player', function(){
    describe('Initialization', function(){
        it('should have the correct X coordinate after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.get_x().should.be.equal(10);
        });
        it('should have the correct Y coordinate after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.get_y().should.be.equal(10);
        });
        it('should have the default angle after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.get_rotation().should.be.equal(0);
        });
        it('should have movement flags set to false after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.left.should.be.equal(false);
            plr.right.should.be.equal(false);
            plr.forward.should.be.equal(false);
            plr.backward.should.be.equal(false);
        });
        it('should be immobile after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
        });
        it('should have no keys after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.keys.length.should.be.equal(0);
        });
    });
    describe('Movement Functions', function(){
        it('should move forward',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.move_forward();
            plr.body.GetLinearVelocity().x.should.be.above(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
        });
        it('should move backward',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.move_backward();
            plr.body.GetLinearVelocity().x.should.be.below(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
        });
        it('should turn right',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.turn_right(5);
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.above(0);
        });
        it('should turn left',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.turn_left(5);
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.below(0);
        });
        it('should slow down',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            for(var i = 0; i < 5; ++i) {
                plr.move_forward();
            }
            var vel_x1 = plr.body.GetLinearVelocity().x;
            plr.slow_down(0.7);
            plr.body.GetLinearVelocity().x.should.be.below(vel_x1);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(0);
        });
    });
    describe('Update Function', function(){
        it('should not move when all the flags are set to false',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.left = false;
            plr.right = false;
            plr.forward = false;
            plr.backward = false;
            for(var i = 0; i < 10; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(0));
        });
        it('should turn left when the left flag is set to true',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.left = true;
            for(var i = 0; i < 10; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(-100));
        });
        it('should turn right when the right flag is set to true',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.right = true;
            for(var i = 0; i < 10; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(100));
        });
        it('should move forward when the forward flag is set to true',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.forward = true;
            for(var i = 0; i < 10; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.above(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(0));
        });
        it('should move backward when the backward flag is set to true',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.backward = true;
            for(var i = 0; i < 10; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.below(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(0));
        });
        it('should stop turning left when the left flag is set back to false',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.left = true;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.left = false;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().toFixed(10).should.be.equal(utils.deg2rad(-50).toFixed(10));
        });
        it('should stop turning right when the right flag is set back to false',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.right = true;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.right = false;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.equal(0);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().toFixed(10).should.be.equal(utils.deg2rad(50).toFixed(10));
        });
        it('should stop moving forward when the forward flag is set back to false',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.forward = true;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            var vel_x1 = plr.body.GetLinearVelocity().x;
            plr.forward = false;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.below(vel_x1);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(0));
        });
        it('should stop moving backward when the backward flag is set back to false',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            plr.body.SetAngle(0);
            plr.backward = true;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            var vel_x1 = plr.body.GetLinearVelocity().x;
            plr.backward = false;
            for(var i = 0; i < 5; ++i) {
                plr.update();
            }
            plr.body.GetLinearVelocity().x.should.be.above(vel_x1);
            plr.body.GetLinearVelocity().y.should.be.equal(0);
            plr.body.GetAngularVelocity().should.be.equal(0);
            plr.get_rotation().should.be.equal(utils.deg2rad(0));
        });
    });
    describe('Key Management', function(){
        it('should have the key after obtaining it',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            var key = new Key(world, 15, 15, 1);
            plr.get_key(key);
            plr.keys.length.should.be.equal(1);
            plr.has_key(1).should.be.equal(true);
        });
        it('should be able to use an obtained key',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            var key = new Key(world, 15, 15, 1);
            plr.get_key(key);
            plr.keys.length.should.be.equal(1);
            plr.use_key(1).should.be.equal(true);
        });
        it('should not be able to use a key without having it',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            var key = new Key(world, 15, 15, 1);
            plr.get_key(key);
            plr.keys.length.should.be.equal(1);
            plr.use_key(2).should.be.equal(false);
        });
        it('should not have the key after using it',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            var key = new Key(world, 15, 15, 1);
            plr.get_key(key);
            plr.keys.length.should.be.equal(1);
            plr.use_key(1);
            plr.keys.length.should.be.equal(0);
            plr.has_key(1).should.be.equal(false);
        });
        it('should not be able to use the same key twice',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var plr = new Player(world, 10, 10);
            var key = new Key(world, 15, 15, 1);
            plr.get_key(key);
            plr.use_key(1);
            plr.use_key(1).should.be.equal(false);
        });
    });
});

