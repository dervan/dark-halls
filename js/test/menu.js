describe('Menu', function(){
    describe('#show', function(){
        it('should not be added to DOM before executing `show` function', function(){
            var menu = document.getElementById('menu');
            (menu === null).should.be.true;
        });
        it('should be in DOM after executing `show` function', function(){
            Menu.show();
            var menu = document.getElementById('menu');
            (menu === null).should.be.false;
            Menu.hide();
        });
        it('should appear only once after calling `show` function multiple times', function(){
            Menu.show();
            Menu.show();
            Menu.show();
            var menu = document.querySelectorAll('[id="menu"]');
            menu.length.should.eql(1);
        });
        it('should add a "New Game" button', function(){
            Menu.show();
            var button = document.getElementById("newGameButton");
            (button === null).should.be.false;
        });
        it('should add a "Credits" button', function(){
            Menu.show();
            var button = document.getElementById("creditsButton");
            (button === null).should.be.false;
        });

    });
    describe('#hide', function(){
        it('should not be is DOM after executing `hide` function', function(){
            Menu.show();
            var menu = document.getElementById('menu');
            (menu === null).should.be.false;
            Menu.hide();
            menu = document.getElementById('menu');
            (menu === null).should.be.true;
        });
        it('should not be is DOM after executing multiple times `show` and once `hide`', function(){
            Menu.show();
            Menu.show();
            Menu.show();
            Menu.hide();
            menu = document.getElementById('menu');
            (menu === null).should.be.true;
        });
    });
});
