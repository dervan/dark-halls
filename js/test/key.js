describe('Key', function(){
    describe('Initialization', function(){
        it('should have the correct X coordinate after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 15, 0);
            key.get_x().should.be.equal(10);
        });
        it('should have the correct Y coordinate after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 15, 0);
            key.get_y().should.be.equal(15);
        });
        it('should have the default angle after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 10, 0);
            key.get_rotation().should.be.equal(0);
        });
        it('should have the correct identification number',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 10, 5);
            key.no.should.be.equal(5);
        });
        it('should not be taken after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 10, 5);
            key.taken.should.be.equal(false);
        });
        it('should not be used after initialization',function(){
            var world = new Box2D.Dynamics.b2World(new Box2D.Common.Math.b2Vec2(0, 0), true);
            var key = new Key(world, 10, 10, 5);
            key.used.should.be.equal(false);
        });
    });
});

