var Player = function(world, x, y) {
    var fixDef = new Box2D.Dynamics.b2FixtureDef();
    fixDef.density = 1.0;
    fixDef.friction = 0.5;
    fixDef.restitution = 0.2;
    fixDef.shape = new Box2D.Collision.Shapes.b2CircleShape(utils.px2m(12));

    var bodyDef = new Box2D.Dynamics.b2BodyDef();
    bodyDef.type = Box2D.Dynamics.b2Body.b2_dynamicBody;
    bodyDef.position.x = x;
    bodyDef.position.y = y;
    bodyDef.rotation = 0;
    bodyDef.fixedRotation = true;
    bodyDef.userData = this;
    this.type = "PLAYER";

    this.keys = new Array();

    this.max_forward_velocity = 75;
    this.max_backward_velocity = 25;
    this.left = false;
    this.right = false;
    this.forward = false;
    this.backward = false;

    this.body = world.CreateBody(bodyDef);
    this.body.CreateFixture(fixDef);
};

_.extend(Player.prototype, {

    get_x: function() {
        return this.body.GetWorldCenter().x;
    },

    get_y: function() {
        return this.body.GetWorldCenter().y;
    },

    get_rotation: function() {
        return this.body.GetAngle();
    },

    get_velocity_length: function() {
        var vel = this.body.GetLinearVelocity();
        return vel.x * vel.x + vel.y * vel.y;
    },

    is_moving: function() {
        return this.get_velocity_length() != 0;
    },

    is_moving_forward: function() {
        return this.forward;
    },

    is_moving_backward: function() {
        return this.backward;
    },

    rotate_velocity_vector: function(deg) {
        var vel = this.body.GetLinearVelocity();
        var vel2 = new Box2D.Common.Math.b2Vec2();
        vel2.x = vel.x * Math.cos(utils.deg2rad(deg)) - vel.y * Math.sin(utils.deg2rad(deg));
        vel2.y = vel.x * Math.sin(utils.deg2rad(deg)) + vel.y * Math.cos(utils.deg2rad(deg));
        this.body.SetLinearVelocity(vel2);
    },

    limit_velocity: function(max_vel) {
        var vel = this.body.GetLinearVelocity();
        var cur_vel = vel.x * vel.x + vel.y * vel.y;
        if(cur_vel > max_vel) {
            var vel2 = new Box2D.Common.Math.b2Vec2();
            vel2.x = (vel.x > 0 ? (Math.abs(vel.x) * max_vel / cur_vel) : -(Math.abs(vel.x) * max_vel / cur_vel));
            vel2.y = (vel.y > 0 ? (Math.abs(vel.y) * max_vel / cur_vel) : -(Math.abs(vel.y) * max_vel / cur_vel));
            this.body.SetLinearVelocity(vel2);
        }
    },

    turn_right: function(deg) {
        this.body.SetAngle(this.body.GetAngle() + utils.deg2rad(deg));
        this.rotate_velocity_vector(deg);
    },

    turn_left: function(deg) {
        this.body.SetAngle(this.body.GetAngle() - utils.deg2rad(deg));
        this.rotate_velocity_vector(-deg);
    },

    move_forward: function() {
        var impulse = new Box2D.Common.Math.b2Vec2(Math.cos(this.get_rotation()), Math.sin(this.get_rotation()));
        this.body.ApplyImpulse(impulse, this.body.GetPosition());
        this.limit_velocity(this.max_forward_velocity);
    },

    move_backward: function() {
        var impulse = new Box2D.Common.Math.b2Vec2(-Math.cos(this.get_rotation()), -Math.sin(this.get_rotation()));
        this.body.ApplyImpulse(impulse, this.body.GetPosition());
        this.limit_velocity(this.max_backward_velocity);
    },

    slow_down: function(times) {
        var vel = this.body.GetLinearVelocity();
        vel.x *= times;
        vel.y *= times;
        this.body.SetLinearVelocity(vel);
    },

    has_key: function(key_no) {
        for(var i = 0; i < this.keys.length; ++i) {
            if(this.keys[i].no == key_no) return true;
        }
        return false;
    },

    use_key: function(key_no) {
        for(var i = 0; i < this.keys.length; ++i) {
            if(this.keys[i].no == key_no) {
                this.keys[i].used = true;
                this.keys.splice(i, 1);
                return true;
            }
        }
        return false;
    },

    get_key: function(key) {
        this.keys.push(key);
    },

    on_collision: function(obj) {
        if(obj === undefined) return;

        if(obj.type === "KEY") {
            this.get_key(obj);
        }

        if(obj.type === "DOOR") {
            // Handled in Door.on_collision()
        }
    },

    update: function() {
        if(this.forward == true) this.move_forward();
        if(this.backward == true) this.move_backward();
        if(this.left == true) this.turn_left(10);
        if(this.right == true) this.turn_right(10);
        if(this.forward == false && this.backward == false) this.slow_down(0.7);
        if(this.right == false && this.left == false) this.body.SetAngularVelocity(0);
    }
});
