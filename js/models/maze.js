// http://www.diffen.com/difference/Labyrinth_vs_Maze
var Maze = function(world, json_maze_description){
    this.world = world;
    if(json_maze_description !== undefined) {
        this.load_maze(json_maze_description);
    }
};
_.extend(Maze.prototype,{
    maze_node: null,
    height: 0,  //It's maximal y coordinate in segments
    width: 0,   //And it's maximal x coordinate
    wall_thickness: 1,
    segments: Array(),
    keys: Array(),
    doors: Array(),
    floor_type: null,
    start_pos: null,
    end_pos: null,

    add_segment: function(segment){
        this.segments.push(segment);
    },
    list_segments: function(){
        return this.segments;
    },
    load_maze: function(json_maze_description){
        this.height = json_maze_description.height;
        this.width = json_maze_description.width;
        this.start_pos = json_maze_description.start_pos;
        this.end_pos = json_maze_description.end_pos;
        this.wall_thickness = json_maze_description.wall_thickness;
        this.segments = json_maze_description.segments;
        this.attach();
        this.add_keys(json_maze_description.keys);
        this.add_doors(json_maze_description.doors);
        if(json_maze_description.floor_type === undefined) {
            this.floor_type = 1;
        }else{
            this.floor_type = json_maze_description.floor_type;
        }
    },
    attach: function(){
        if(this.world === undefined) {
            console.warn("Cannot attach maze -- world undefined");
            return;
        }
        for(var i=0; i<this.segments.length; i++){
            var segment = this.segments[i];

            var groundBodyDef = new Box2D.Dynamics.b2BodyDef();
            groundBodyDef.position.x = (segment[0][0] + segment[1][0])/2;
            groundBodyDef.position.y = (segment[0][1] + segment[1][1])/2;
            var groundFixDef = new Box2D.Dynamics.b2FixtureDef();
            groundFixDef.density = 1.0;
            groundFixDef.friction = 0.5;
            groundFixDef.restitution = 0.2;
            groundFixDef.shape = new Box2D.Collision.Shapes.b2PolygonShape();

            if(segment[0][0] == segment[1][0]) { // vertical wall
                groundFixDef.shape.SetAsBox(
                        this.wall_thickness/2,
                        Math.abs(segment[0][1] - segment[1][1])/2 + this.wall_thickness/2);
            }else if(segment[0][1] == segment[1][1]) { // horizontal wall
                groundFixDef.shape.SetAsBox(
                        Math.abs(segment[0][0] - segment[1][0])/2 + this.wall_thickness/2,
                        this.wall_thickness/2);
            }else{
                console.warn("WARNING: non-axis-aligned walls not supperted");
            }
            this.world.CreateBody(groundBodyDef).CreateFixture(groundFixDef);
        }
    },

    add_keys: function(key_array) {
        if(this.world === undefined) {
            console.warn("Cannot add keys -- world undefined");
            return;
        }
        for(var i = 0; i < key_array.length; ++i) {
            this.keys.push(new Key(this.world, key_array[i][0], key_array[i][1], key_array[i][2]));
        }
    },

    add_doors: function(door_array) {
        if(this.world === undefined) {
            console.warn("Cannot add doors -- world undefined");
            return;
        }
        for(var i = 0; i < door_array.length; ++i) {
            this.doors.push(new Door(this.world, door_array[i][0], door_array[i][1], door_array[i][2]));
        }
    },

    update: function() {
        var i;
        for(i = this.keys.length - 1; i >= 0; --i) {
            if(this.keys[i].taken) {
                this.keys[i].destroy_key();
                this.keys.splice(i, 1);
            }
        }
        for(i = this.doors.length - 1; i >= 0; --i) {
            if(this.doors[i].opened) {
                this.doors[i].destroy_door();
                this.doors.splice(i, 1);
            }
        }
    }
});
