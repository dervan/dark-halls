var Door = function(world, x, y, no) {
    this.world = world;
    var fixDef = new Box2D.Dynamics.b2FixtureDef();
    fixDef.density = 1.0;
    fixDef.friction = 0.5;
    fixDef.restitution = 0.2;
    fixDef.shape = new Box2D.Collision.Shapes.b2PolygonShape;
    fixDef.shape.SetAsBox(utils.px2m(12), utils.px2m(12));

    var bodyDef = new Box2D.Dynamics.b2BodyDef();
    bodyDef.position.x = x;
    bodyDef.position.y = y;
    bodyDef.rotation = 0;
    bodyDef.userData = this;
    this.type = "DOOR";

    this.body = world.CreateBody(bodyDef);
    this.body.CreateFixture(fixDef);
    this.no = no;
    this.opened = false;  // A box2d object should not be removed during world.Step
};

_.extend(Door.prototype, {

    get_x: function() {
        return this.body.GetWorldCenter().x;
    },

    get_y: function() {
        return this.body.GetWorldCenter().y;
    },

    get_rotation: function() {
        return this.body.GetAngle();
    },

    destroy_door: function() {
        this.world.DestroyBody(this.body);
    },

    on_collision: function(obj) {
        if(obj === undefined) return;

        if(obj.type === "PLAYER") {
            if(obj.use_key(this.no)) this.opened = true;
        }
    },

    update: function() {
    }
});
