var Viewport = function(follow) {
    this.follow = follow;
    this.scale = 1;
    this.x = 0; //position
    this.y = 0;
    this.vx = 0; //velocity
    this.vy = 0;
    this.k = 0.07; //speed factor
    this.scale = 1; //scale
    if(follow !== undefined) {
        this.x = follow.get_x();
        this.y = follow.get_y();
    }
};

_.extend(Viewport.prototype, {
    update: function() {
        if(this.follow !== undefined) {       
            this.vx = (this.follow.get_x() - this.x) * this.k;
            this.vy = (this.follow.get_y() - this.y) * this.k;
        }else{
            this.vx *= this.k;
            this.vy *= this.k;
        }
        this.x += this.vx;
        this.y += this.vy;
    },
    getWidth: function() {
        return window.innerWidth;
    },
    getHeight: function() {
        return window.innerHeight;
    },
    topLeft: function() {
        return [(utils.m2px(this.x) - this.getWidth()/2),
                (utils.m2px(this.y) - this.getHeight()/2)];
    }
});
