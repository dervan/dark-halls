test:
	mocha-phantomjs ./tests.html --reporter spec

test-cov:

	$(info )
	$(info Go to http://127.0.0.1:8080/jscoverage.html?tests.html to see coverage report)
	$(info Close the server with ctrl+c)

	jscoverage-server --encoding=utf8 --no-instrument /js/vendor

spritesheet:
	python2 ./utils/glue/glue.py --json img/sprites ./img/

runserver:
	python2 -m SimpleHTTPServer 8000

build:
	sh ./utils/build.sh
	
.PHONY: test test-cov spritesheet runserver build 
